#!/bin/bash

# rndrootdir/op1Emz9/HvgXT7o/####jeSd94Li.txt has "John"

t_file="ex1.tar.gz"
for i in `tar -tf $t_file | grep -E "(\#\#\#\#[A-Za-z0-9].+).txt"`
do
    echo "$t_file $i"
    tar -xvf $t_file $i

    if grep -E 'John' $i ; then 
        rm -rf $i
    fi
    # tar -xf $t_file $i | grep -Hai '(^John)'
 done
